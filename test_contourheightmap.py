# pytest
import numpy as np
from pathlib import Path
import tempfile

from contourheightmap import (
    rgb2gray,
    ContourHeightmap
)


def test_rgb2gray():
    data = np.array([[0.1, 0.1, 0.1]])
    assert rgb2gray(data) == np.array([0.09999])


def test_load_heightmap():
    c = ContourHeightmap()
    c.load_heightmap("examples/heightmap.png")
    assert c.width == 100
    assert c.height == 100


def test_prepare_heightmap():
    c = ContourHeightmap()
    c.load_heightmap("examples/heightmap.png")
    c.prepare_heightmap()


def test_contour():
    c = ContourHeightmap()
    with tempfile.TemporaryDirectory() as tmpdirname:
        filename = Path(tmpdirname) / "output.png"
        c.contour("examples/heightmap.png", filename)

        assert filename.exists() is True
        assert filename.with_suffix(".svg").exists() is True

